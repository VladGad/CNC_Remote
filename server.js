// Подключаем модуль websocket
const { Console } = require('console');
const WebSocket = require('ws');

// Создаем новый WebSocket-сервер
const wss = new WebSocket.Server({ port: 80 });

// При подключении нового клиента
wss.on('connection', function connection(ws) {
    console.log("Приветики")
  // При получении сообщений от клиента
  ws.on('message', function incoming(message) {
    var parsed = JSON.parse(message);

   // console.log(Date.now());
    //console.log(parsed.date);
    console.log('Received message:', parsed.data, Date.now() - parsed.date);
  //  ws.send(Date.now())
    ws.send(JSON.stringify({action: 'RESPONSE', data: parsed.data, date: Date.now() }))
  });

  // При закрытии соединения с клиентом
  ws.on('close', function close() {
    console.log('Client disconnected');
  });

  // Отправляем сообщение клиенту
 // ws.send('Hello, client!');
  
});

console.log('WebSocket server listening on port 8080');