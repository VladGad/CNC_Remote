const WebSocket = require('ws');
const myWs = new WebSocket('ws://7.tcp.eu.ngrok.io:17915');
// обработчик проинформирует в консоль когда соединение установится
myWs.onopen = function () {
  console.log('подключился');
 restart();
};
// обработчик сообщений от сервера
myWs.onmessage = function (message) {
  var parsed = JSON.parse(message.data);
  //console.log('Message:', message.data);
  
  console.log('Message: %s',parsed.data, parsed.date -  Date.now());
};
// функция для отправки echo-сообщений на сервер
function wsSendEcho(value) {
  myWs.send(JSON.stringify({action: 'G-Code', data: value.toString(), date: Date.now() }));
}
// функция для отправки команды ping на сервер
function wsSendPing() {
  myWs.send(JSON.stringify({action: 'PING'}));
}

function restart() {
    setTimeout(restart, 500); // Перезапустить функцию через 500 миллисекунд (полсекунды)
    wsSendEcho("G01 X247.951560 Y11.817060 Z-1.000000 F400.000000");
  }

function getCurrentData(){
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    today = mm + '/' + dd + '/' + yyyy;
    return today
}